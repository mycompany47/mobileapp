import React, { useEffect, useState } from 'react';

import { StyleSheet, Text, View, FlatList, TextInput, ScrollView, TouchableOpacity, AppRegistry } from 'react-native';
import { ListItem, Avatar, Button } from '@rneui/themed'



import axios, { all } from 'axios';

export default function App() {

  const [cards, setCards] = useState([]);
  const [filteredCards, setFilteredCards] = useState([]);
  const [error, setError] = useState(false)
  const [showDefaultList, setShowDefaultList] = useState(true)

  const [search, setSearch] = useState("");

  const updateSearch = async (search) => {
    console.log("this is filter here--------", filteredCards)
    if(search === ""){
      setShowDefaultList(true)
      setError(false)
    }else {
      setShowDefaultList(false)
    }
    let searchedData = search.toLowerCase();
    try {
      const getsearch = await axios.get(
        `https://openlibrary.org/search.json?title=${searchedData}`
      );
      if(getsearch.data.docs.length == 0) {
        setError(true)
      } 
      else setError(false)
      setFilteredCards(getsearch.data.docs);
    } catch (error) {
        setError(true)
      console.log("err", error);
    }
  };


  const toggleBook = (index) => {
    setCards((prevCards) => {
      return prevCards.map((element) => {
        if (element.work.cover_id === index) {
          element.work.isRead = !element.work.isRead;
        }
        return element;
      });
    });
  };




  const getData = async () => {
    const arr = []
    try {
      const response = await axios.get("https://openlibrary.org/people/mekBot/books/already-read.json")
      setCards(response.data.reading_log_entries.slice(0, 20) || []);

    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getData()
  }, [])



  return (

    <>
      <View style={styles.header}>
        <TextInput
          style={styles.searchBar}
          onChangeText={updateSearch}
          placeholder="Search"
        />
      </View>
      <ScrollView>
        <View>

          {showDefaultList ?

            <View>
              {cards.map((element, index) => {
                return (
                  <ListItem key={index} bottomDivider>
                    <Avatar source={{ uri: `https://covers.openlibrary.org/b/id/${element.work.cover_id}-M.jpg` }} />
                    <ListItem.Content>
                      <ListItem.Title>{element.work.title}</ListItem.Title>
                      <ListItem.Subtitle>author: {element.work.author_names}</ListItem.Subtitle>
                      <ListItem.Subtitle>published: {element.work.first_publish_year}</ListItem.Subtitle>
                      <Button
                        {...(element.work.isRead
                          ? { type: 'solid', title: "Read" }
                          : { type: "outline", title: "UnRead" })}
                        onPress={() => toggleBook(element.work.cover_id)}
                      />
                    </ListItem.Content>
                  </ListItem>
                );
              })}
            </View> :
            <View>

              {error ?
                  <Text>Not Found</Text>
                  :
                <>
                  {filteredCards.map((element, index) => {
                    return (
                      <>
                        <ListItem key={index} bottomDivider>
                          <ListItem.Content>
                            <ListItem.Title>{element.title_suggest}</ListItem.Title>
                            <ListItem.Subtitle>
                              author: {element.author_name}
                            </ListItem.Subtitle>
                            <ListItem.Subtitle>
                              published: {element.first_publish_year}
                            </ListItem.Subtitle>
                          </ListItem.Content>
                        </ListItem>
                      </>
                    );
                  })}
                </> 

              }
            </View>

          }






        </View>
      </ScrollView>




    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    backgroundColor: '#3498db',
    padding: 16,
    height: 80,
    marginTop: 20,
  },
  searchBar: {
    backgroundColor: '#fff',
    padding: 8,
    marginRight: 16,
    borderRadius: 4,
  },
  button: {
    backgroundColor: '#3498db',
    padding: 5,
    width: "50%",
    borderRadius: 5,
  },
  buttonUnread: {
    backgroundColor: 'transparent',
    padding: 5,
    width: "50%",
    borderRadius: 5,
    borderColor: "red",
    borderWidth: "2"
  },
  buttonText: {
    color: 'black',
    fontSize: 12,
    textAlign: 'center',
  },
});
